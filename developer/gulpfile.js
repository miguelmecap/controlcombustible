const gulp = require('gulp'),
  gulpSass = require('gulp-sass'),
  concat = require('gulp-concat'),
  minificarCss = require('gulp-clean-css'),
  plumber = require('gulp-plumber'),
  renombrar= require('gulp-rename'),
  minificarJs = require('gulp-uglify'),
  autoprefixer = require('gulp-autoprefixer'),
  babel = require('gulp-babel'),
  cssnano = require('gulp-cssnano');

gulp.task('js', ()=> {
  gulp.src('js/*.js')
    .pipe(plumber())
    .pipe(babel())
    .pipe(concat('main.js'))
    .pipe(minificarJs())
    .pipe(renombrar('scripts.min.js'))
    .pipe(gulp.dest('../public/js/'))
});

gulp.task('css',()=>{
  gulp.src(['scss/main.scss'])
    .pipe(plumber())
    .pipe(gulpSass({
      outputStyle:'expanded'
    }))
    .pipe(autoprefixer({
      versions: ['last 2 browsers']
    }))
    .pipe(renombrar('styles.css'))
    .pipe(minificarCss())
    .pipe(cssnano())
    .pipe(gulp.dest('../public/css/'))
});

gulp.task('default', ['js','css', 'watch']);

gulp.task('watch', function() {
  gulp.watch('js/**/*.js', ['js']);
  gulp.watch('scss/**/*.scss', ['css']);
});
