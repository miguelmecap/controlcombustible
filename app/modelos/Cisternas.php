<?php

class Cisternas
{

  private $db;
  private static $instancia;

  private function __construct()
  {
    $this->db = Base::getInstance();
  }

  public static function getInstance()
  {

    if (!isset(self::$instancia)) {
      $miclase = __class__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }

  public function listarCisternasActivas()
  {
    $this->db->query('SELECT no_valor1,no_valor2 FROM mae_tabla_detalle WHERE id_tabla_general=127 AND fl_inactivo=0');
    return $this->db->registros();
  }

  public function listarMovimientosCisternas()
  {
    $this->db->query('select fe_hr_movimiento,
    (select no_valor1 from mae_tabla_detalle where no_valor2=id_cisterna and id_tabla_general=127) as cisterna,
    cantidad_abastecida
    from comb_movimiento_cisterna');
    return $this->db->registros();
  }

  public function insertarMovimientoCisterna($datos)
  {
    try {
      $this->db->beginTransaction();
      $this->db->query('INSERT INTO comb_movimiento_cisterna(id_cisterna, cantidad_abastecida,fe_hr_movimiento,fe_crea,co_usuario_crea) VALUES(?,?,?,?,?)');
      $params = array(
        $datos['cisterna'],
        $datos['cantidad'],
        $datos['fechaHora'],
        $datos['fechaHora'],
        $datos['user']
      );
      $this->db->execute1($params);
      $this->db->commit();
      return true;
    } catch (Exception $ex) {
      $this->db->rollBack();
      return "Error: " . $ex->getMessage();
    }
  }

  // Evita que el objeto se pueda clonar
  public function __clone()
  {
    trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
  }


}
