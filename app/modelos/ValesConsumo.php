<?php

class ValesConsumo
{

  private $db;
  private static $instancia;

  private function __construct()
  {
    $this->db = Base::getInstance();
  }

  public static function getInstance()
  {

    if (!isset(self::$instancia)) {
      $miclase = __class__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }

  public function listarValesConsumo()
  {
    return array('cantidad' => 1);
  }

  public function listarAlmacenesCombustibles()
  {
    $this->db->query('SELECT no_valor1,no_valor2 FROM mae_tabla_detalle WHERE id_tabla_general=128 AND fl_inactivo=0');
    return $this->db->registros();
  }

  public function listarValesEmpresa()
  {
    $this->db->query('SELECT no_valor2,no_valor1 FROM mae_tabla_detalle WHERE id_tabla_general=130 AND fl_inactivo=0');
    return $this->db->registros();
  }

  public function listarLugarDespacho()
  {
    $this->db->query('SELECT no_valor2,no_valor1 FROM mae_tabla_detalle WHERE id_tabla_general=129 AND fl_inactivo=0');
    return $this->db->registros();
  }

  public function listarAreasNegocio()
  {
    $this->db->query('SELECT id_area_negocio, no_area_negocio FROM  mae_area_negocio WHERE fl_inactivo=0 AND fl_combustible = 0');
    return $this->db->registros();
  }

  public function listarSubAreasNegocioByIdArea($idAreaNegocio = 2)
  {
    $this->db->query("SELECT
    san.id_detalle,
    san.id_area_negocio,
    san.id_subgrupo,
    an.fl_tipo_subgrupo,
    CASE an.fl_tipo_subgrupo
    WHEN '001' THEN emb.no_embarcacion
    WHEN '002' THEN veh.nu_placa
    WHEN '003' THEN san.no_subgrupo
    END sub_area_negocio
    FROM
    mae_area_negocio_subgrupo san
    INNER JOIN mae_area_negocio an ON an.id_area_negocio = san.id_area_negocio
    LEFT JOIN mae_embarcacion emb ON emb.id_embarcacion = san.id_subgrupo
    LEFT JOIN mae_vehiculo veh ON veh.id_vehiculo = san.id_subgrupo
    WHERE san.fl_inactivo = '0' AND san.fl_bloqueo = '0'
    AND san.id_area_negocio =:idArea ORDER BY sub_area_negocio");
    $this->db->bind(':idArea', $idAreaNegocio);
    $this->db->execute();
    return $this->db->registros();
  }

  /*public function listarCisternasActivas()
  {
    $this->db->query('SELECT no_valor1,no_valor2 FROM mae_tabla_detalle WHERE id_tabla_general=127 AND fl_inactivo=0');
    return $this->db->registros();
  }

  public function listarMovimientosCisternas()
  {
    $this->db->query('select fe_hr_movimiento,
    (select no_valor1 from mae_tabla_detalle where no_valor2=id_cisterna and id_tabla_general=127) as cisterna,
    cantidad_abastecida
    from comb_movimiento_cisterna');
    return $this->db->registros();
  }

  public function insertarMovimientoCisterna($datos)
  {
    try {
      $this->db->beginTransaction();
      $this->db->query('INSERT INTO comb_movimiento_cisterna(id_cisterna, cantidad_abastecida,fe_hr_movimiento,fe_crea,co_usuario_crea) VALUES(?,?,?,?,?)');
      $params = array(
        $datos['cisterna'],
        $datos['cantidad'],
        $datos['fechaHora'],
        $datos['fechaHora'],
        $datos['user']
      );
      $this->db->execute1($params);
      $this->db->commit();
      return true;
    } catch (Exception $ex) {
      $this->db->rollBack();
      return "Error: " . $ex->getMessage();
    }
  }*/

  // Evita que el objeto se pueda clonar
  public function __clone()
  {
    trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
  }


}
