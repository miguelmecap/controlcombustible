<?php

class Reportes
{

  private $db;
  private static $instancia;

  private function __construct()
  {
    $this->db = Base::getInstance();
  }

  public static function getInstance()
  {

    if (!isset(self::$instancia)) {
      $miclase = __class__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }

  public function stockActualCisternas()
  {
    $this->db->query('SELECT no_valor1,no_valor3 FROM mae_tabla_detalle WHERE id_tabla_general=127 AND fl_inactivo=0');
    $respuesta = $this->db->registros();

    $cisternas = array();
    foreach ($respuesta as $row => $item) {
      $registro = array(
        'nombreCisterna' => $item->no_valor1,
        'stock' => $item->no_valor3
      );
      array_push($cisternas, (object)$registro);
    }

    return $cisternas;

  }

  public function stockActualAlmacenes()
  {
    $this->db->query('SELECT no_valor1,no_valor3 FROM mae_tabla_detalle WHERE id_tabla_general=128 AND fl_inactivo=0');
    $respuesta = $this->db->registros();

    $almacenes = array();
    foreach ($respuesta as $row => $item) {
      $registro = array(
        'nombreAlmacen' => $item->no_valor1,
        'stock' => $item->no_valor3
      );
      array_push($almacenes, (object)$registro);
    }

    return $almacenes;

  }

}
