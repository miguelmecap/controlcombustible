<?php

class AreaNegocio
{

  private $db;
  private static $instancia;

  private function __construct()
  {
    $this->db = Base::getInstance();
  }

  public static function getInstance()
  {

    if (!isset(self::$instancia)) {
      $miclase = __class__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }

  public function listarAreasNegocio()
  {
    $this->db->query('SELECT id_area, no_area FROM mae_area');
    return $this->db->registros();
  }

  // Evita que el objeto se pueda clonar
  public function __clone()
  {
      trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
  }


}
