<?php

class Contometro
{
  private $db;
  private static $instancia;

  private function __construct()
  {
    $this->db = Base::getInstance();
  }

  public static function getInstance()
  {

    if (!isset(self::$instancia)) {
      $miclase = __class__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }

  public function listarContometrosActivos()
  {
    $this->db->query('SELECT no_valor1,no_valor2, no_valor3, no_valor4 FROM mae_tabla_detalle WHERE id_tabla_general=129 AND fl_inactivo=0');
    return $this->db->registros();
  }

}
