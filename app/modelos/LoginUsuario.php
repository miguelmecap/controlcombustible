<?php

class LoginUsuario
{

  private $db;
  private static $instancia;

  private function __construct()
  {
    $this->db = Base::getInstance();
  }

  public static function getInstance()
  {

    if (!isset(self::$instancia)) {
      $miclase = __class__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }

  public function validarUsuario($datos)
  {
    $this->db->query('SELECT id_usuario,no_usuario, no_email, fl_bloqueado FROM mae_usuario_sistema WHERE no_login=:user AND no_pass=:clave');
    $this->db->bind(':user', $datos['usuario']);
    $this->db->bind(':clave', $datos['password']);
    $this->db->execute();
    return $this->db->registro();
  }

  public function verificaCookie($datos)
  {
    $this->db->query('SELECT count(id_usuario) FROM mae_usuario_sistema_cookie WHERE id_usuario=:user AND cookie=:marca');
    $this->db->bind(':user', $datos['id']);
    $this->db->bind(':marca', $datos['marca']);
    $this->db->execute();
    return $this->db->fetch();
  }

  public function insertarCookie($datos)
  {
    mt_srand(time());
    $rand = mt_rand(1000000, 9999999);
    $fecha = date("Y-m-d H:i:s");
    try {
      $this->db->beginTransaction();
      $this->db->query('INSERT INTO mae_usuario_sistema_cookie(id_usuario, cookie,fe_crea) VALUES(?,?,?)');
      $params = array(
        $datos['idUser'],
        $rand,
        $fecha
      );
      if ($this->db->execute1($params)) {
        setcookie("id_user", $datos['idUser'], time() + (60 * 60 * 24 * 365));
        setcookie("marca", $rand, time() + (60 * 60 * 24 * 365));
      }
      $this->db->commit();
    } catch (Exception $ex) {
      $this->db->rollBack();
      return "Error: " . $ex->getMessage();
    }
  }

  // Evita que el objeto se pueda clonar
  public function __clone()
  {
    trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
  }

}
