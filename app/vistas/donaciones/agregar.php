<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="content">
  <div class="row">
  <div class="col-sm-12 col-md-1">
  </div>
    <div class="col-sm-12 col-md-9">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-12 col-md-8">
              <strong class="card-title">Nueva Donación</strong>
            </div>
            <div class="col-sm-12 col-md-4">
              <a href="<?php echo RUTA_URL; ?>/Donacion/" class="btn btn-info float-right">Regresar</a>
            </div>
          </div>
        </div>
        <div class="card-body card-block">
          <form method="post" id="js-formDonacion">
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <label class="control-label mb-1">Fecha</label>
                  <div class="input-group">
                    <input type="text" id="datepicker" class="form-control" size="10">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-3 col-md-5">
              <div class="form-group">
                <label for="js-nomAlm" class="control-label mb-1">Origen Del Petróleo</label>
                <select name="nom_almacen" id="js-nomAlm" class="form-control">
                <?php
                foreach ($datos['almacen'] as $almacen) {
                  ?>
                  <option value="<?php echo $almacen->no_valor2; ?>"><?php echo $almacen->no_valor1; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label class=" form-control-label">Empresa/Persona Beneficiada</label>
                  <input type="text" name="beneficiado" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-md-4">
                <div class="form-group">
                  <label class=" form-control-label">Unidad</label>
                  <select name="tipoVale" id="js-unidad" class="form-control">
                    <?php
                    foreach ($datos['tipoUnidad'] as $tipo) {
                      ?>
                        <option value="<?php echo $tipo->no_valor2; ?>"><?php echo $tipo->no_valor1; ?></option>
                        <?php

                      }
                      ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-12 col-md-4">
                <div class="form-group">
                  <label class=" form-control-label">Vehíc/Embarcación</label>
                 <input type="text" name="vehiculo" class="form-control">
                </div>
              </div>
              <div class="col-sm-12 col-md-4">
                <div class="form-group">
                  <label class=" form-control-label">Placa/Matrícula</label>
                  <input type="text" name="placa" class="form-control">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 col-md-4">
                <div class="form-group">
                  <label class=" form-control-label">Cant. Despachada</label>
                  <input type="number" name="cantDespachada" class="form-control" min="0" value="0">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label class=" form-control-label">Confirmado por</label>
                  <input type="text" name="confirmado" class="form-control">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label class=" form-control-label">Doc.Persona  Confirma</label>
                  <input type="text" name="docConfirma" class="form-control">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 col-md-12">
                <div class="form-group">
                  <div class="input-group">
                    <input type="file" id="username2" name="username2" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label class="form-control-label">Observaciones</label>
                  <textarea class="form-control" name="observacion"></textarea>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 col-md-8">
                <button id="js-grabaValeInterno" type="submit" class="btn btn-lg btn-success btn-block">
                  <i class="fa fa-lock fa-lg"></i>&nbsp;
                  <span id="payment-button-amount">Grabar</span>
                  <span id="payment-button-sending" style="display:none">Sending…</span>
                </button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
