<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="content">
  <div class="animated fadeIn">
    <div class="row">
    <div class="col-sm-12 col-md-3">
    </div>
      <div class="col-sm-12 col-md-6">
        <div class="card">
          <div class="card-header">
          <div class="row">
            <div class="col-sm-12 col-md-8">
              <strong class="card-title">Agregar Medición de Contómetro</strong>
            </div>
            <div class="col-sm-12 col-md-4">
              <a href="<?php echo RUTA_URL; ?>/contometros/" class="btn btn-info float-right">Regresar</a>
            </div>
          </div>
          </div>
          <div class="card-body card-block">
            <form action="<?php echo RUTA_URL; ?>/contometros/grabar" method="post" class="">
              <div class="form-group">
                <label for="select" class=" form-control-label">Contómetro</label>
                <select name="nom_contometro" class="form-control" id="js-contometro">
                <option value="00" data-medAnt="0" data-fechAnt="0" >-- Seleccione --</option>
                <?php
                foreach ($datos['contometros'] as $contometro) {
                  ?>
                  <option value="<?php echo $contometro->no_valor2;
                                ?>" data-medAnt="<?php echo $contometro->no_valor3; ?>" data-fechAnt="<?php echo $contometro->no_valor4; ?>"><?php echo $contometro->no_valor1; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              <div class="form-group">
                <label class="form-control-label">Medida Anterior</label>
                <input type="text" name="medida_anterior" class="form-control" id="js-medidAnt">
              </div>
              <div class="form-group">
                <label class="form-control-label">Medida Anterior</label>
                <input type="text"name="medida_ant" id='js-medidaAnterior' class="form-control" readonly>
              </div>
              <div class="form-group">
                <label for="js-cantidad" class=" form-control-label">Medida Actual</label>
                <input type="number" id="js-cantidad" name="cant_abastecida" class="form-control" min="0" value="0">
                <?php
                if (isset($datos['error'])) {
                  ?>
                  <span class="help-block"><?php echo $datos['error']; ?></span>
                <?php

              }
              ?>
              </div>
              <div class="row">
                <div class="col-6">
                  <div class="form-group" id="basicExample">
                    <label for="cc-exp" class="control-label mb-1">Fecha Medición</label>
                    <input id="datepicker" name="cc-exp" type="text" class="form-control cc-exp date">
                      <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <label for="cc-exp" class="control-label mb-1">Hora Medición</label>
                    <input  name="cc-exp" id="setTimeExample" type="text" class="form-control cc-exp">
                      <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                  </div>
                </div>
              </div>

            </div>
              <div>
                <button id="payment-button" type="submit" class="btn btn-lg btn-success btn-block">
                  <i class="fa fa-lock fa-lg"></i>&nbsp;
                  <span id="payment-button-amount">Grabar</span>
                  <span id="payment-button-sending" style="display:none">Sending…</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-3">
    </div>
    </div>
  </div>
</div>
<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
