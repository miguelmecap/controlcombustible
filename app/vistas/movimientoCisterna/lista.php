<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-12">
        <div class="page-header">
          <div class="page-title">
            <h1 class="text-center">Movimientos De Petróleo En Cisternas</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-12 col-md-8">
              <strong class="card-title">Table Head</strong>
            </div>
            <div class="col-sm-12 col-md-4">
              <a href="<?php echo RUTA_URL; ?>/MovimientoCisterna/agregar" class="btn btn-success float-right">Agregar Movimiento</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Fecha y Hora</th>
                <th scope="col">Cisterna</th>
                <th scope="col">Cantidad Abastecida</th>
                <th scope="col">Detalle</th>
              </tr>
            </thead>
            <tbody>
            <?php
            foreach ($datos['movimientos'] as $movimiento) {
              ?>
            <tr>
              <th scope="row"><?php echo $movimiento->fe_hr_movimiento; ?></th>
              <td><?php echo $movimiento->cisterna; ?></td>
              <td><?php echo $movimiento->cantidad_abastecida; ?></td>
              <td><?php echo $movimiento->cantidad_abastecida; ?></td>
          </tr>
            <?php

          }
          ?>
            </tbody>
          </table>
        </div>
       </div>
    </div>
  </div>
</div>

<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
