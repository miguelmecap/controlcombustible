<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="content">
  <div class="animated fadeIn">
    <div class="row">
    <div class="col-sm-12 col-md-3">
    </div>
      <div class="col-sm-12 col-md-6">
        <div class="card">
          <div class="card-header">
          <div class="row">
            <div class="col-sm-12 col-md-8">
              <strong class="card-title">Agregar Movimiento en Cisterna</strong>
            </div>
            <div class="col-sm-12 col-md-4">
              <a href="<?php echo RUTA_URL; ?>/MovimientoCisterna/" class="btn btn-info float-right">Regresar</a>
            </div>
          </div>
          </div>
          <div class="card-body card-block">
            <form action="<?php echo RUTA_URL; ?>/MovimientoCisterna/grabar" method="post" class="">
              <div class="form-group">
                <label for="select" class=" form-control-label">Cisterna</label>
                <select name="nom_cisterna" id="select" class="form-control">
                <?php
                foreach ($datos['cisternas'] as $cisterna) {
                  ?>
                  <option value="<?php echo $cisterna->no_valor2; ?>"><?php echo $cisterna->no_valor1; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              <div class="form-group">
                <label for="js-cantidad" class=" form-control-label">Cantidad</label>
                <input type="number" id="js-cantidad" name="cant_abastecida" class="form-control" min="0" value="0">
                <?php
                if (isset($datos['error'])) {
                  ?>
                  <span class="help-block"><?php echo $datos['error']; ?></span>
                <?php

              }
              ?>

              </div>
              <div>
                <button id="payment-button" type="submit" class="btn btn-lg btn-success btn-block">
                  <i class="fa fa-lock fa-lg"></i>&nbsp;
                  <span id="payment-button-amount">Grabar</span>
                  <span id="payment-button-sending" style="display:none">Sending…</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-3">
    </div>
    </div>
  </div>
</div>
<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
