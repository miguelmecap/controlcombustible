<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="author" content="Miguel Angel Meca Pacherres">
  <meta name="description" content="Software de pedidos para restaurantes, lleva el control de tus pedidos en mesa,delivery.">
  <link rel="icon" type="image/png" href="views/img/brasas.png">
  <title>
    <?php echo NOMBRE_SITIO; ?>
  </title>
  <link rel="stylesheet" href="<?php echo RUTA_URL ?>/public/lib/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL ?>/public/lib/dataTable/css/dataTables.jqueryui.min.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL ?>/public/lib/dataTable/responsive/css/responsive.jqueryui.min.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL ?>/public/lib/easyResponsiveTabs/css/easy-responsive-tabs.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL ?>/public/css/sweetalert.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL ?>/public/css/styles.css">
  <script src="<?php echo RUTA_URL ?>/public/js/sweetalert.min.js"></script>
</head>

<body class="bg-body-login">
  <div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
      <div class="login-content">
        <div class="login-logo">
          <img class="align-content" src="<?php echo RUTA_URL_PUBLICA; ?>/img/logo.png" alt="">
        </div>
        <div class="login-form">
          <form action="<?php echo RUTA_URL; ?>/login/validarIngreso" method="post" onsubmit="return validaIngresoLogin()">
            <div class="form-group">
              <label>Usuario</label>
              <input type="text" class="form-control" placeholder="Usuario" name="user" id="js-user" require>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" placeholder="Password" name="clave" id="js-clave" require>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name='recordarLogin' value="S"> Recordarme Usuario y Clave
              </label>
            </div>
            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Ingresar</button>
            <div class="register-link m-t-15 text-center">
              <p>
                <?php echo NOMBRE_APLICATIVO . ' Versión ' . VERSION; ?>
              </p>
            </div>
            <p>
              <?php echo empty($datos['mensaje']) ? '' : $datos['mensaje']; ?>
            </p>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="<?php echo RUTA_URL; ?>/public/lib/jquery/jquery-1.12.4.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/lib/jquery-ui/jquery-ui.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/lib/dataTable/js/jquery.dataTables.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/lib/dataTable/js/dataTables.jqueryui.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/lib/dataTable/responsive/js/dataTables.responsive.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/lib/dataTable/responsive/js/responsive.jqueryui.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/lib/easyResponsiveTabs/js/easyResponsiveTabs.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/lib/chartjs/Chart.min.js">
  </script>
  <script src="<?php echo RUTA_URL; ?>/public/js/scripts.min.js">
  </script>
</body>

</html>
