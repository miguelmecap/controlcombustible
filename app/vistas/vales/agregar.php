<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="content">
  <div class="animated fadeIn">
    <div class="row">
    <div class="col-sm-12 col-md-3">
    </div>
      <div class="col-sm-12 col-md-6">
        <div class="card">
          <div class="card-header">
          <div class="row">
            <div class="col-sm-12 col-md-8">
              <strong class="card-title">Nuevo Vale Interno</strong>
            </div>
            <div class="col-sm-12 col-md-4">
              <a href="<?php echo RUTA_URL; ?>/ValeConsumo/" class="btn btn-info float-right">Regresar</a>
            </div>
          </div>
          </div>
          <div class="card-body card-block">
            <form method="post" id="js-formVale">
              <div class="form-group">
                <label for="js-nomAlm" class=" form-control-label">Origen Del Petróleo</label>
                <select name="nom_almacen" id="js-nomAlm" class="form-control">
                <?php
                foreach ($datos['almacen'] as $almacen) {
                  ?>
                  <option value="<?php echo $almacen->no_valor2; ?>"><?php echo $almacen->no_valor1; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              <div class="form-group">
                <label for="js-tipoVale" class=" form-control-label">Vale de:</label>
                <select name="tipoVale" id="js-tipoVale" class="form-control">
                <?php
                foreach ($datos['tipoVale'] as $tipo) {
                  ?>
                  <option value="<?php echo $tipo->no_valor2; ?>"><?php echo $tipo->no_valor1; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              <div class="row">
                <div class="col-6">
                  <div class="form-group" id="basicExample">
                    <label for="cc-exp" class="control-label mb-1">Fecha</label>
                    <input id="datepicker" name="cc-exp" type="text" class="form-control cc-exp date">
                      <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class=" form-control-label">Lugar de despacho:</label>
                <select name="tipoVale" id="js-lugarDespacho" class="form-control">
                <?php
                foreach ($datos['lugarDespacho'] as $lugar) {
                  ?>
                  <option value="<?php echo $lugar->no_valor2; ?>"><?php echo $lugar->no_valor1; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              <div class="form-group">
                <label class=" form-control-label">Unidad:</label>
                <select name="areaNegocio" id="js-areaNegocio" class="form-control">
                <?php
                foreach ($datos['areaNegocio'] as $area) {
                  ?>
                  <option value="<?php echo $area->id_area_negocio; ?>"><?php echo $area->no_area_negocio; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              <div class="form-group">
                <label class=" form-control-label">Vehículo/Embarcación:</label>
                <select name="subArea" id="js-subArea" class="form-control">
                <?php
                foreach ($datos['subAreasNegocio'] as $subArea) {
                  ?>
                  <option value="<?php echo $subArea->id_detalle; ?>"><?php echo $subArea->sub_area_negocio; ?></option>
                  <?php

                }
                ?>
                </select>
              </div>
              <div class="form-group">
                <label class=" form-control-label">Placa/Matrícula</label>
                <input type="text" id="js-cantidad" name="cant_abastecida" class="form-control">
              </div>
              <div class="form-group">
                <label class=" form-control-label">Cantidad</label>
                <input type="number" id="js-cantidad" name="cant_abastecida" class="form-control" min=0>
              </div>
              <div class="form-group">
                <label class=" form-control-label">Destino</label>
                <input type="text" name="cant_abastecida" class="form-control">
              </div>
              <div class="form-group">
                <label class=" form-control-label">Km Anterior</label>
                <input type="text" name="cant_abastecida" class="form-control" readonly>
              </div>

              <div class="form-group">
                <label class=" form-control-label">Km Actual</label>
                <input type="text" name="cant_abastecida" class="form-control">
              </div>
              <div class="form-group">
                <label class=" form-control-label">Confirmado por</label>
                <input type="text" name="cant_abastecida" class="form-control">
              </div>
              <div class="form-group">
                <label class=" form-control-label">DNI Confirma</label>
                <input type="text" name="cant_abastecida" class="form-control">
              </div>
              <div class="form-group">
                <label class=" form-control-label">Observaciones</label>
                <textarea class="form-control" name=observacion></textarea>
              </div>

              <div>
                <button id="js-grabaValeInterno" type="submit" class="btn btn-lg btn-success btn-block" >
                  <i class="fa fa-lock fa-lg"></i>&nbsp;
                  <span id="payment-button-amount">Grabar</span>
                  <span id="payment-button-sending" style="display:none">Sending…</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-3">
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="smallmodalLabel">Small Modal</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                There are three species of zebras: the plains zebra, the mountain zebra and the Grévy's zebra. The plains zebra
                                and the mountain zebra belong to the subgenus Hippotigris, but Grévy's zebra is the sole species of subgenus
                                Dolichohippus. The latter resembles an ass, to which it is closely related, while the former two are more
                                horse-like. All three belong to the genus Equus, along with other living equids.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>

<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
