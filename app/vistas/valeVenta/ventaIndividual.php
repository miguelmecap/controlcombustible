<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="content">
  <div class="row">
  <div class="col-sm-12 col-md-3">
  </div>
  <div class="col-sm-12 col-md-6">
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-sm-12 col-md-8">
            <strong class="card-title">Nuevo Vale Venta de Petróleo</strong>
          </div>
          <div class="col-sm-12 col-md-4">
            <a href="<?php echo RUTA_URL; ?>/ValeVenta/" class="btn btn-info float-right">Regresar</a>
          </div>
        </div>
      </div>
      <div class="card-body card-block">
        <form method="post" id="js-formValeVentaIndividual">
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label class="control-label mb-1">Fecha</label>
                <input id="datepicker" name="fechaVenta" type="text" class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class=" form-control-label">N° Factura</label>
                <input type="text" name="numFactura" class="form-control">
          </div>
          <div class="form-group">
            <label class=" form-control-label">Cliente</label>
                <input type="text" name="cliente" class="form-control">
          </div>
          <div class="form-group">
            <label class=" form-control-label">N° Doc. Cliente</label>
                <input type="text" name="numDocCliente" class="form-control">
          </div>
          <div class="form-group">
            <label class=" form-control-label">Unidad</label>
            <select name="tipoVale" id="js-unidad" class="form-control">
              <?php
              foreach ($datos['tipoUnidad'] as $tipo) {
                ?>
                  <option value="<?php echo $tipo->no_valor2; ?>"><?php echo $tipo->no_valor1; ?></option>
                  <?php

                }
                ?>
            </select>
          </div>
          <div class="form-group">
            <label class=" form-control-label">Vehíc/Embarcación</label>
            <input type="text" name="vehiculo" class="form-control">
          </div>
          <div class="form-group">
            <label class=" form-control-label">Placa/Matrícula</label>
            <input type="text" name="placa" class="form-control">
          </div>
          <div class="form-group">
            <label class=" form-control-label">Cant. Facturada</label>
            <input type="number" name="cantFacturada" class="form-control" min=0 value=0>
          </div>
          <div class="form-group">
            <label class=" form-control-label">Cant. Despachada</label>
            <input type="number" name="cantDespachada" class="form-control" min=0 value=0>
          </div>
          <div class="form-group">
            <label class=" form-control-label">Confirmado por</label>
            <input type="text" name="confirmado" class="form-control">
          </div>
          <div class="form-group">
            <label class=" form-control-label">DNI Confirma</label>
            <input type="text" name="docConfirma" class="form-control">
          </div>
          <div class="form-group">
            <label class="form-control-label">Observaciones</label>
            <textarea class="form-control" name=observacion></textarea>
          </div>
          <div>
            <button id="js-grabaValeInterno" type="submit" class="btn btn-lg btn-success btn-block" >
              <i class="fa fa-lock fa-lg"></i>&nbsp;
              <span id="payment-button-amount">Grabar</span>
              <span id="payment-button-sending" style="display:none">Sending…</span>
            </button>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
