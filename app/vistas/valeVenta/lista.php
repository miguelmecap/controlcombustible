<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-12">
        <div class="page-header">
          <div class="page-title">
            <h1 class="text-center">Vales de Venta</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-12 col-md-8">
              <strong class="card-title">Aqui van los campos de busqueda</strong>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-6">
            </div>
            <div class="col-sm-12 col-md-3">
            <a href="<?php echo RUTA_URL; ?>/ValeVenta/ventaIndividual" class="btn btn-success float-right">Venta Individual</a>
            </div>
            <div class="col-sm-12 col-md-3">
              <a href="<?php echo RUTA_URL; ?>/ValeVenta/ventaValeConsumo" class="btn btn-success float-right">Venta por Vales Consumo</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Número</th>
                <th scope="col">Estado</th>
                <th scope="col">Fecha</th>
                <th scope="col">Cliente</th>
                <th scope="col">Tipo</th>
                <th scope="col">Vehículo/Embarcación</th>
                <th scope="col">Placa/Matrícula</th>
                <th scope="col">Factura</th>
                <th scope="col">Opciones</th>
              </tr>
            </thead>
            <tbody>
            <?php
            foreach ($datos['valesVenta'] as $vale) {
              ?>
            <tr><!--
              <th scope="row"><?php /*echo $movimiento->fe_hr_movimiento; */ ?></th>
              <td></td>
              <td></td>
              <td></td>-->
          </tr>
            <?php

          }
          ?>
            </tbody>
          </table>
        </div>
       </div>
    </div>
  </div>
</div>

<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
