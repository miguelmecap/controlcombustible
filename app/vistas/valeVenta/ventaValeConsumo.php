<?php require_once RUTA_APP . '/vistas/inc/seguridad.php'; ?>
<?php require_once RUTA_APP . '/vistas/inc/header.php'; ?>

<div class="content">
  <div class="row">
    <div class="col-sm-12 col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-12 col-md-8">
              <strong class="card-title">Generar Venta por Vale de Consumo Interno</strong>
            </div>
            <div class="col-sm-12 col-md-4">
              <a href="<?php echo RUTA_URL; ?>/ValeVenta/" class="btn btn-info float-right">Regresar</a>
            </div>
          </div>
        </div>
        <div class="card-body card-block">
          <div class="row">
          <div class="col-lg-12">
            <div class="default-tab">
              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Consultar Vales Consumo</a>
                </div>
              </nav>
              <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  <div class="form-inline">
                    <div class="form-group">
                      <label class="pr-1  form-control-label">Vale de:</label>
                      <select name="tipoVale" id="js-tipoVale" class="form-control">
                        <?php
                        foreach ($datos['tipoVale'] as $tipo) {
                          ?>
                          <option value="<?php echo $tipo->no_valor2; ?>"><?php echo $tipo->no_valor1; ?></option>
                          <?php

                        }
                        ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="px-1 form-control-label"> Fecha</label>
                      <div class="input-group">
                        <input type="text" id="fechaValeConsumo"   class="form-control" size="10">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="form-control-label"> </label>
                      <button type="button" class="btn btn-primary">Consultar</button>
                    </div>
                  </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-12">
            <label class="switch switch-3d switch-primary mr-3">Seleccionar Todos <input type="checkbox" class="switch-input" checked="true">
              </label>
            </div>
            <div class="col-sm-12 col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col"><input type="checkbox" class="switch-input" checked="true"></th>
                  <th scope="col">Unidad</th>
                  <th scope="col">Descripcion</th>
                  <th scope="col">Cant. Desp.</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row"><input type="checkbox" class="switch-input" checked="true"></th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                </tr>
              </tbody>
            </table>
            </div>
            <div class="col-sm-12 col-md-3">
              <div class="form-group">
                  <label class=" form-control-label">Nº Factura</label>
                  <input type="text" name="factura" class="form-control">
              </div>
            </div>

          </div>
          <div class="row">
          <div class="col-sm-12 col-md-6">
              <div class="form-group">
                <label class=" form-control-label">Observaciones</label>
                <textarea class="form-control" name=observacion></textarea>
              </div>
            </div>
          </div>
          <div class="row">
          <button id="js-grabaValeInterno" type="submit" class="btn btn-lg btn-success btn-block" >
                  <i class="fa fa-lock fa-lg"></i>&nbsp;
                  <span id="payment-button-amount">Grabar</span>
                  <span id="payment-button-sending" style="display:none">Sending…</span>
                </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once RUTA_APP . '/vistas/inc/footer.php'; ?>
