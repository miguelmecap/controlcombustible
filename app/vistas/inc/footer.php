<!-- /.content -->
<div class="clearfix"></div>
<!-- Footer -->
<footer class="site-footer">
  <div class="footer-inner bg-white">
    <div class="row">
      <div class="col-sm-6">
        Copyright &copy; 2018 Ela Admin
      </div>
      <div class="col-sm-6 text-right">
        Designed by <a href="https://colorlib.com">Colorlib</a>
      </div>
    </div>
  </div>
</footer>
</div>
<!-- Scripts -->
<script src="<?php echo RUTA_URL_PUBLICA; ?>/js/jquery.min.js">
</script>
<script src="<?php echo RUTA_URL_PUBLICA; ?>/js/popper.min.js">
</script>
<script src="<?php echo RUTA_URL_PUBLICA; ?>/js/bootstrap.min.js">
</script>
<script src="<?php echo RUTA_URL_PUBLICA; ?>/js/main.js">
</script>
<script src="<?php echo RUTA_URL_PUBLICA; ?>/js/jquery.matchHeight.min.js">
</script>
<script src="<?php echo RUTA_URL_PUBLICA; ?>/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo RUTA_URL_PUBLICA; ?>/js/scripts.min.js">
</script>

</body>

</html>
