<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo NOMBRE_SITIO; ?></title>
  <meta name="description" content="Ela Admin - HTML5 Admin Template">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo RUTA_URL_PUBLICA; ?>/img/QRAUqs9.png">
  <link rel="stylesheet" href="<?php echo RUTA_URL_PUBLICA; ?>/css/normalize.min.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL_PUBLICA; ?>/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.min.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL_PUBLICA; ?>/lib/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL_PUBLICA; ?>/css/styles.css">
  <style>
    #weatherWidget .currentDesc {
      color: #ffffff!important;
    }
    .traffic-chart {
      min-height: 335px;
    }
    #flotPie1  {
      height: 150px;
    }
    #flotPie1 td {
      padding:3px;
    }
    #flotPie1 table {
      top: 20px!important;
      right: -10px!important;
    }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }

    </style>
</head>
<body class="small-device">
<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li >
                        <a href="<?php echo RUTA_URL; ?>/dashboard"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li class="menu-title">UI elements</li><!-- /.menu-title -->
                    <li>
                        <a href="<?php echo RUTA_URL; ?>/MovimientoCisterna"> <i class="menu-icon fa fa-cogs"></i>Movimiento Cisternas</a>
                       <!--
                        <ul class="sub-menu children dropdown-menu">     <li><i class="fa fa-puzzle-piece"></i><a href="ui-buttons.html">Buttons</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Badges</a></li>
                        </ul>
                        -->
                    </li>
                    <li>
                        <a href="<?php echo RUTA_URL; ?>/Contometros"> <i class="menu-icon fa fa-cogs"></i>Medición Contómetros</a>
                    </li>

                    <li>
                        <a href="<?php echo RUTA_URL; ?>/ValeConsumo"> <i class="menu-icon fa fa-table"></i>Vales de Consumo interno</a>
                    </li>

                    <li>
                        <a href="<?php echo RUTA_URL; ?>/ValeVenta"> <i class="menu-icon fa fa-table"></i>Vales de Venta</a>
                    </li>

                    <li>
                        <a href="<?php echo RUTA_URL; ?>/Donacion"> <i class="menu-icon fa fa-table"></i>Donaciones</a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <div id="right-panel" class="right-panel">
    <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo RUTA_URL; ?>/dashboard"><img src="<?php echo RUTA_URL; ?>/img/logo.png" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="<?php echo RUTA_URL; ?>/dashboard"><img src="<?php echo RUTA_URL; ?>/img/logo2.png" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left centrado">
                    <label class="no-margin-bottom">Bienvenido <?php echo $_SESSION['nombreUser']; ?></label>
                    </div>

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="<?php echo RUTA_URL_PUBLICA; ?>/img/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                            <a class="nav-link" href="<?php echo RUTA_URL ?>/login/logout"><i class="fa fa-power -off"></i>Salir</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>
