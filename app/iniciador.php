<?php
date_default_timezone_set("America/Lima");
//Cargamos las librerias
require_once 'config/configurar.php';
require_once 'helpers/url_helper.php';

//Autoload php
spl_autoload_register(function ($nombreClase) {
  require_once 'Core/' . $nombreClase . '.php';
});
