<?php

class ValeConsumo extends Controlador
{

  public function __construct()
  {
    $this->valeConsumoModel = $this->modelo('ValesConsumo');
  }

  public function index()
  {

    $data = [
      "vales" => $this->valeConsumoModel->listarValesConsumo()
    ];
    $this->vista('vales/lista', $data);

  }

  public function agregar()
  {
    $data = [
      "almacen" => $this->valeConsumoModel->listarAlmacenesCombustibles(),
      "tipoVale" => $this->valeConsumoModel->listarValesEmpresa(),
      "lugarDespacho" => $this->valeConsumoModel->listarLugarDespacho(),
      "areaNegocio" => $this->valeConsumoModel->listarAreasNegocio(),
      "subAreasNegocio" => $this->valeConsumoModel->listarSubAreasNegocioByIdArea()
    ];
    $this->vista('vales/agregar', $data);
  }

  public function mostrarSubAreaNegocio($idAreaNegocio)
  {
    $datos = [
      'subAreasNegocio' => $this->valeConsumoModel->listarSubAreasNegocioByIdArea($idAreaNegocio)
    ];
    //foreach ($datos['subAreasNegocio'] as $subArea) {
    $cadena = "";
    foreach ($datos['subAreasNegocio'] as $subArea) {
      $cadena .= '<option value="' . $subArea->id_detalle . '">' . $subArea->sub_area_negocio . '</option>';
    }

    echo $cadena;

  }

  public function grabar()
  {
    echo "ok";
    /*if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $cisterna = $_POST['nom_cisterna'];
      $cantidad = $_POST['cant_abastecida'];
      $fechaHora = date("Y-m-d H:i:s");
      if (!isset($_SESSION)) {
        session_start();
      }
      $usuario = $_SESSION['userLogin'];

      if ($cantidad > 0) {
        $datos = [
          'cisterna' => $cisterna,
          'cantidad' => $cantidad,
          'fechaHora' => $fechaHora,
          'user' => $usuario
        ];
        if ($this->dashboardModel->insertarMovimientoCisterna($datos)) {
          redireccionar('/MovimientoCisterna/lista');
        }
      } else {
        $data = [
          "cisternas" => $this->dashboardModel->listarCisternasActivas(),
          "error" => "La cantidad debe ser mayor a cero"
        ];
        $this->vista('/MovimientoCisterna/agregar', $data);
      }
    } else {
      $data = [
        "cisternas" => $this->dashboardModel->listarCisternasActivas()
      ];
      $this->vista('/MovimientoCisterna/agregar', $data);
    }*/
  }

}
