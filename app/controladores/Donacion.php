<?php

class Donacion extends Controlador
{
  public function __construct()
  {
    $this->donacionModel = $this->modelo('Donaciones');
    $this->valeVentaModel = $this->modelo('ValesVenta');
    $this->valeConsumoModel = $this->modelo('ValesConsumo');
  }

  public function index()
  {

    $data = [
      "donaciones" => $this->donacionModel->listarDonaciones()
    ];

    $this->vista('donaciones/lista', $data);

  }

  public function agregar()
  {
    $data = [
      "almacen" => $this->valeConsumoModel->listarAlmacenesCombustibles(),
      "tipoUnidad" => $this->valeVentaModel->listarTipoUnidades()
    ];
    $this->vista('donaciones/agregar', $data);
  }

  /*public function ventaIndividual()
  {
    $data = [
      "tipoUnidad" => $this->valeVentaModel->listarTipoUnidades()
    ];
    $this->vista('valeVenta/ventaIndividual', $data);
  }

  public function ventaValeConsumo()
  {
    $data = [
      "tipoVale" => $this->valeConsumoModel->listarValesEmpresa()
    ];
    $this->vista('valeVenta/ventaValeConsumo', $data);
  }*/


}
