<?php

class Login extends Controlador
{
  private $loginModel;

  public function __construct()
  {
    $this->loginModel = $this->modelo('LoginUsuario');
  }

  public function index()
  {
    /*if (isset($_COOKIE['id_user']) && isset($_COOKIE['marca'])) {
      if ($_COOKIE['id_user'] != "" || $_COOKIE['marca'] != "") {
        $datos = [
          'id' => $_COOKIE['id_user'],
          'marca' => $_COOKIE['marca']
        ];
        if (self::verificaCookie($datos)) {
          $this->vista('dashboard');
        } else {
          $this->vista('login');
        }
      } else {
        $this->vista('login');
      }
    } else {
      $this->vista('login');
    }*/
    $this->vista('login');
  }

  public function validarIngreso()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $user = trim($_POST['user']);
      $clave = trim($_POST['clave']);
      $recordar = isset($_POST['recordarLogin']) ? trim($_POST['recordarLogin']) : 'N';

      if (!empty($user) && !empty($clave)) {
        $datos = [
          'usuario' => $user,
          'password' => md5($clave)
        ];

        $dataRecibida = $this->loginModel->validarUsuario($datos);
        if (!empty($dataRecibida)) {
          if ($dataRecibida->fl_bloqueado == 'N') {
            if ($recordar == 'S') {
              $datosUserLogin = [
                'idUser' => $dataRecibida->id_usuario
              ];
              $this->loginModel->insertarCookie($datosUserLogin);
            }
            session_start();
            $_SESSION['userLogin'] = $user;
            $_SESSION['nombreUser'] = $dataRecibida->no_usuario;
            $_SESSION['emailUser'] = $dataRecibida->no_email;
            $_SESSION['check'] = $recordar;
            redireccionar('/Dashboard');
          } else {
            $datos = [
              'usuario' => '',
              'password' => '',
              'mensaje' => 'El usuario se encuentra bloqueado'
            ];
            $this->vista('/login', $datos);
          }
        } else {
          $datos = [
            'usuario' => '',
            'password' => '',
            'mensaje' => 'No existe usuario'
          ];
          $this->vista('/login', $datos);
        }
      } else {
        $this->vista('/login');
      }
    } else {
      $datos = [
        'usuario' => '',
        'password' => ''
      ];
      $this->vista('/login', $datos);
    }
  }

  public function verificaCookie($datos)
  {
    $dataRecibida = $this->loginModel->verificaCookie($datos);
    if ($dataRecibida > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function logout()
  {
    if (!isset($_SESSION)) {
      session_start();
    }
    unset($_SESSION['nombreUser']);
    unset($_SESSION['emailUser']);
    unset($_SESSION['userLogin']);
    redireccionar('/login');
  }

}
