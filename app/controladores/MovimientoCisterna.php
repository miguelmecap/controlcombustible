<?php

class MovimientoCisterna extends Controlador
{

  public function __construct()
  {
    $this->dashboardModel = $this->modelo('Cisternas');
  }

  public function index()
  {
    $data = [
      "movimientos" => $this->dashboardModel->listarMovimientosCisternas()
    ];
    $this->vista('movimientoCisterna/lista', $data);
  }

  public function agregar()
  {
    $data = [
      "cisternas" => $this->dashboardModel->listarCisternasActivas()
    ];
    $this->vista('movimientoCisterna/agregar', $data);
  }

  public function grabar()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $cisterna = $_POST['nom_cisterna'];
      $cantidad = $_POST['cant_abastecida'];
      $fechaHora = date("Y-m-d H:i:s");
      if (!isset($_SESSION)) {
        session_start();
      }
      $usuario = $_SESSION['userLogin'];

      if ($cantidad > 0) {
        $datos = [
          'cisterna' => $cisterna,
          'cantidad' => $cantidad,
          'fechaHora' => $fechaHora,
          'user' => $usuario
        ];
        if ($this->dashboardModel->insertarMovimientoCisterna($datos)) {
          redireccionar('/MovimientoCisterna/lista');
        }
      } else {
        $data = [
          "cisternas" => $this->dashboardModel->listarCisternasActivas(),
          "error" => "La cantidad debe ser mayor a cero"
        ];
        $this->vista('/MovimientoCisterna/agregar', $data);
      }
    } else {
      $data = [
        "cisternas" => $this->dashboardModel->listarCisternasActivas()
      ];
      $this->vista('/MovimientoCisterna/agregar', $data);
    }
  }

}
