<?php

class Dashboard extends Controlador
{

  public function __construct()
  {
    $this->dashboardModel = $this->modelo('Reportes');
  }

  public function index()
  {
    $stockCisterna = $this->dashboardModel->stockActualCisternas();
    $stockAlmacenes = $this->dashboardModel->stockActualAlmacenes();

    $data = [
      'cisterna' => $stockCisterna,
      'almacenes' => $stockAlmacenes
    ];

    $this->vista('dashboard', $data);
  }

  public function movimientoCisterna()
  {
    $this->vista('movimientoCisterna');
  }
}
