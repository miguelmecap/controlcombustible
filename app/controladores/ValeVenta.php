<?php

class ValeVenta extends Controlador
{
  public function __construct()
  {
    $this->valeVentaModel = $this->modelo('ValesVenta');
    $this->valeConsumoModel = $this->modelo('ValesConsumo');
  }

  public function index()
  {

    $data = [
      "valesVenta" => $this->valeVentaModel->listarValesVenta()
    ];

    $this->vista('valeVenta/lista', $data);

  }

  public function ventaIndividual()
  {
    $data = [
      "tipoUnidad" => $this->valeVentaModel->listarTipoUnidades()
    ];
    $this->vista('valeVenta/ventaIndividual', $data);
  }

  public function ventaValeConsumo()
  {
    $data = [
      "tipoVale" => $this->valeConsumoModel->listarValesEmpresa()
    ];
    $this->vista('valeVenta/ventaValeConsumo', $data);
  }


}
