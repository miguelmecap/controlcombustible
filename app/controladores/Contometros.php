<?php

class Contometros extends Controlador
{
  public function __construct()
  {
    $this->contometroModel = $this->modelo('Contometro');
  }

  public function index()
  {
    /*$data = [
      "movimientos" => $this->dashboardModel->listarMovimientosCisternas()
    ];*/
    $this->vista('contometros/lista');
  }

  public function agregar()
  {
    $data = [
      "contometros" => $this->contometroModel->listarContometrosActivos()
    ];
    $this->vista('contometros/agregar', $data);
  }

}
