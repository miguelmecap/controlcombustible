<?php

/*Mapear la URL ingresada en el navegador web
 1- controlador
 2-metodo
 3-parametro
 Ejm: /articulos/actualizar/4
*/

class Core{

  protected $controladorActual = 'login';
  protected $metodoActual = 'index';
  protected $parametros = [];

  public function __construct()
  {
      $url = $this->getUrl();

      if(file_exists('../app/controladores/' . ucwords($url[0]) . '.php')){
        $this->controladorActual = ucwords($url[0]);
        unset($url[0]);
      }

      require_once '../app/controladores/' . $this->controladorActual . '.php';
      $this->controladorActual = new $this->controladorActual;

      if(isset($url[1])){
        if(method_exists($this->controladorActual, $url[1])){
          $this->metodoActual = $url[1];
          unset($url[1]);
        }
      }

      $this->parametros = $url ? array_values($url) : [];
      //Llamar callback con parametros array
      call_user_func_array([$this->controladorActual, $this->metodoActual],$this->parametros);
  }

  public function getUrl(){
    //Esta url es del archivo htaccess que esta en la carpeta public
    //echo $_GET['url'];
    if(isset($_GET['url'])){
      //Cortamos los espacios que pueda tener el / en la url
      $url = rtrim($_GET['url'],'/');
      //Para que sea interpretado o leido como una url
      $url = filter_var($url,FILTER_SANITIZE_URL);
      $url = explode('/',$url);
      return $url;
    }
  }

}
