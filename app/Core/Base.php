<?php

class Base
{

  private $usuario = DB_USUARIO;
  private $password = DB_PASSWORD;

  private $dbh;
  private $stmt;
  private $error;

  // Contenedor de la instancia del singleton
  private static $instancia;

  // Un constructor privado evita la creación de un nuevo objeto
  private function __construct()
  {
    try {
      $this->dbh = new PDO(URI, $this->usuario, $this->password);
      //$this->dbh->exec('SET NAMES utf8');
    } catch (PDOException $e) {
      echo $this->error;
    }
  }

  // método singleton
  public static function getInstance()
  {
    if (!isset(self::$instancia)) {
      $miclase = __class__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }

  public function beginTransaction()
  {
    $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->dbh->beginTransaction();
  }

  public function commit()
  {
    $this->dbh->commit();
  }

  public function rollBack()
  {
    $this->dbh->rollBack();
  }

  public function query($sql)
  {
    $this->stmt = $this->dbh->prepare($sql);
  }

  public function bind($parametro, $valor, $tipoDato = null)
  {
    if (is_null($tipoDato)) {
      switch (true) {
        case is_int($valor):
          $tipo = PDO::PARAM_INT;
          break;
        case is_bool($valor):
          $tipo = PDO::PARAM_BOOL;
          break;
        case is_null($valor):
          $tipo = PDO::PARAM_NULL;
          break;
        default:
          $tipo = PDO::PARAM_STR;
          break;
      }
    }
    $this->stmt->bindValue($parametro, $valor, $tipo);
  }

  public function execute()
  {
    return $this->stmt->execute();
  }

  public function execute1($params)
  {
    return $this->stmt->execute($params);
  }

  public function registros()
  {
    $this->execute();
    return $this->stmt->fetchAll(PDO::FETCH_OBJ);
  }

  public function registro()
  {
    $this->execute();
    return $this->stmt->fetch(PDO::FETCH_OBJ);
  }

  public function fetch()
  {
    return $this->stmt->fetchColumn();
  }

  public function rowCount()
  {
    return $this->stmt->rowCount();
  }

  // Evita que el objeto se pueda clonar
  public function __clone()
  {
    trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
  }


}
